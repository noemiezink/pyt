import unittest
from decimal import Decimal

from weight.weight import WeightUnit, Weight


class TestWeight(unittest.TestCase):

    def test_construction(self):
        weight = Weight('3.95')
        assert weight.payload == Decimal('3.95')
        assert weight.unit == WeightUnit.KILO

    def test_zero(self):
        weight = Weight()
        assert weight.payload == Decimal('0.00')
        assert weight.unit == WeightUnit.KILO
