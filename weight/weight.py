from decimal import Decimal, ROUND_HALF_UP
from enum import Enum


class WeightUnit(Enum):
    """
    Gewichtseinheiten, welche wir in den folgenden Definitionen
    von Klassen und Methoden verwenden werden. Bei diesen Angaben
    gehen wir davon aus, dass das Basisgewicht in KILO angegeben
    wird.
    """

    KILO = 1.0
    GRAMM = 0.001
    PFUND = 0.5


# noinspection PyMethodFirstArgAssignment,PyTypeChecker
class Weight:
    """
    Mit dieser Klasse wollen wir Gewichtsobjekte verwalten und anschliessend
    arithmetische Operationen zulassen.
    """

    def __init__(self, payload: str = 0.0, unit: WeightUnit = WeightUnit.KILO) -> None:
        """
        Instanziieren eines Geldobjektes.

        :param payload: absoluter Betrag
        :param unit: Gewichtseinheit
        """

        self.__payload = self._round(Decimal(payload))
        self.__unit = unit

    @property
    def payload(self) -> Decimal:
        """Returns the numeric amount"""
        return self.__payload

    @property
    def unit(self) -> WeightUnit:
        """Returns the currency"""
        return self.__unit

    @staticmethod
    def _round(payload: Decimal) -> Decimal:
        return payload.quantize(Decimal(str(1 / 100)), rounding=ROUND_HALF_UP)

    def convert_to_kg(self) -> 'Weight':
        """Normalisiert das Gewicht in kg"""
        amount = self.__payload * Decimal(self.__unit.value)
        amount = self._round(amount)
        return Weight(amount, WeightUnit.KILO)

    def __add__(self, other: 'Weight') -> 'Weight':
        """
        Addition zweier Gewichte. Wenn die beiden Gewichte
        nicht der selben Einheit entsprechen, dann werden
        diese auf KILO normalisiert.
        """

        if self.__unit != other.__unit:
            self = self.convert_to_kg()
            other = other.convert_to_kg()

        return self.__class__(str(self.__payload + other.__payload), self.__unit)

    def __radd__(self, other: 'Weight') -> 'Weight':
        return self.__add__(other)

    def __repr__(self):
        """Interne Repräsentation des Gewichtes."""
        return f"Weight({self.__payload}, {self.__unit})"

    def __str__(self):
        """Externe Repräsentation des Gewichtes."""
        return self.__repr__()


if __name__ == '__main__':
    weight = Weight(1.1, WeightUnit.GRAMM)
    weight = weight.convert_to_kg()

    weight1 = Weight(1.1, WeightUnit.GRAMM)
    weight2 = Weight(2.3, WeightUnit.GRAMM)

    print(weight1 + weight2)
